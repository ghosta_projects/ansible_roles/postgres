Postgres role
=========

This role will install PostgreSQL different versions on ubuntu.

Requirements
------------

Ansible modules:
  - community.general
  - community.postgresql

Dependencies
------------

No dependencies

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - postgresql
